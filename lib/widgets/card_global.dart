import 'package:covid_app/models/global_summary.dart';
import 'package:covid_app/utils/theme.dart';
import 'package:flutter/material.dart';
import 'dart:core';

class CardGlobal extends StatelessWidget {
  final GlobalSummaryModel summary;
  CardGlobal({@required this.summary});
  @override
  Widget build(BuildContext context) {
    Widget buildCard(
        String title, int totalCount, int todayCount, Color color) {
      return Card(
        elevation: 1,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(
            10,
          ),
        ),
        child: Container(
          padding: EdgeInsets.only(
            top: 1,
            left: 10,
            right: 10,
            bottom: 10,
          ),
          height: 70,
          child: Column(
            children: [
              Text(
                title,
                style: defaultTextStyle.copyWith(
                  color: Colors.grey,
                  fontSize: 12,
                ),
              ),
              Expanded(
                child: Container(),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Total",
                        style: defaultTextStyle.copyWith(
                          fontSize: 13,
                          color: color,
                        ),
                      ),
                      Text(
                        totalCount.toString().replaceAllMapped(reg, mathFunc),
                        style: TextStyle(
                          fontSize: 16,
                          color: color,
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        "Today",
                        style: defaultTextStyle.copyWith(
                          fontSize: 13,
                          color: color,
                        ),
                      ),
                      Text(
                        todayCount.toString().replaceAllMapped(reg, mathFunc),
                        style: TextStyle(
                          fontSize: 16,
                          color: color,
                        ),
                      ),
                    ],
                  )
                ],
              )
            ],
          ),
        ),
      );
    }

    return Scaffold(
      body: Column(
        children: [
          buildCard(
            "CONFIRM",
            summary.totalConfirmed,
            summary.newConfirmed,
            redColor,
          ),
          buildCard(
            "ACTIVE",
            summary.totalConfirmed -
                summary.totalRecovered -
                summary.totalDeaths,
            summary.newConfirmed - summary.newRecovered - summary.newDeaths,
            blueColor,
          ),
          buildCard(
            "RECOVERED",
            summary.totalRecovered,
            summary.newRecovered,
            greenColor,
          ),
          buildCard(
            "DEATH",
            summary.totalDeaths,
            summary.newDeaths,
            greyColor,
          ),
        ],
      ),
    );
  }
}
