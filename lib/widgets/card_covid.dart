import 'package:covid_app/models/global_summary.dart';
import 'package:covid_app/pages/home_page.dart';
import 'package:covid_app/widgets/card_global.dart';
import 'package:covid_app/widgets/loading_shimmer.dart';
import 'package:flutter/material.dart';

class CardCovid extends StatefulWidget {
  @override
  _CardCovidState createState() => _CardCovidState();
}

class _CardCovidState extends State<CardCovid> {
  Future<GlobalSummaryModel> summary;

  @override
  void initState() {
    super.initState();
    summary = covidServices.getGlobalSummary();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: summary,
      builder: (context, snapshot) {
        if (snapshot.hasError)
          return Center(
            child: Text("error"),
          );
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                LoadingShimmer(),
              ],
            ));
            break;
          default:
            return !snapshot.hasData
                ? Center(
                    child: Text("ecmpty"),
                  )
                : CardGlobal(summary: snapshot.data);
        }
      },
    );
  }
}
