import 'package:covid_app/models/country_summary.dart';
import 'package:covid_app/utils/theme.dart';
import 'package:flutter/material.dart';

class CardSearchPage extends StatelessWidget {
  final List<CountrySummaryModel> summaryList;

  CardSearchPage({@required this.summaryList});

  @override
  Widget build(BuildContext context) {
    Widget buildCard(String leftTitle, int leftValue, Color leftColor,
        String rightTitle, int rightValue, Color rightColor) {
      return Card(
        elevation: 1,
        child: Container(
          height: 70,
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    leftTitle,
                    style: defaultTextStyle.copyWith(
                      color: Colors.grey,
                      fontSize: 10,
                    ),
                  ),
                  Expanded(
                    child: Container(),
                  ),
                  Text(
                    "Total",
                    style: defaultTextStyle.copyWith(
                      fontSize: 13,
                      color: leftColor,
                    ),
                  ),
                  Text(
                    leftValue.toString().replaceAllMapped(reg, mathFunc),
                    style: TextStyle(
                      fontSize: 16,
                      color: leftColor,
                    ),
                  ),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Text(
                    rightTitle,
                    style: defaultTextStyle.copyWith(
                      color: Colors.grey,
                      fontSize: 10,
                    ),
                  ),
                  Expanded(
                    child: Container(),
                  ),
                  Text(
                    "Total",
                    style: defaultTextStyle.copyWith(
                      fontSize: 13,
                      color: rightColor,
                    ),
                  ),
                  Text(
                    rightValue.toString().replaceAllMapped(reg, mathFunc),
                    style: TextStyle(
                      fontSize: 16,
                      color: rightColor,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      );
    }

    return Container(
      margin: EdgeInsets.only(
        left: 25,
        right: 25,
      ),
      child: Column(
        children: [
          buildCard(
            "CONFIRMED",
            summaryList[summaryList.length - 1].confirmed,
            redColor,
            "ACTIVE",
            summaryList[summaryList.length - 1].active,
            blueColor,
          ),
          buildCard(
            "RECOVERED",
            summaryList[summaryList.length - 1].recovered,
            greenColor,
            "DEATH",
            summaryList[summaryList.length - 1].death,
            greyColor,
          ),
        ],
      ),
    );
  }
}
