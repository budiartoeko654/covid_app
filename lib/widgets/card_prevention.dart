import 'package:covid_app/utils/theme.dart';
import 'package:flutter/material.dart';

class CardPrevention extends StatelessWidget {
  final String pic;
  final String title;
  final String text;

  CardPrevention(
    this.pic,
    this.title,
    this.text,
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        left: 25,
        right: 25,
        top: 20,
        bottom: 10,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 10),
            blurRadius: 10,
            color: Colors.white,
          ),
        ],
      ),
      child: Row(
        children: [
          Image.asset(
            pic,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                title,
                style: defaultTextStyle.copyWith(
                  fontWeight: FontWeight.w600,
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: 20,
                  vertical: 15,
                ),
                width: MediaQuery.of(context).size.width - 230,
                child: Text(
                  text,
                  style: defaultTextStyle.copyWith(
                    fontSize: 13,
                    color: greyColor,
                  ),
                  textAlign: TextAlign.center,
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
