import 'package:covid_app/models/country.dart';
import 'package:covid_app/models/country_summary.dart';
import 'package:covid_app/services/covid_services.dart';
import 'package:covid_app/utils/theme.dart';
import 'package:covid_app/widgets/card_searchPage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

CovidServices covidServices = CovidServices();

class CardSearch extends StatefulWidget {
  @override
  _CardSearchState createState() => _CardSearchState();
}

class _CardSearchState extends State<CardSearch> {
  final TextEditingController _typeController = TextEditingController();
  Future<List<CountryModel>> countryList;
  Future<List<CountrySummaryModel>> summaryList;

  List<String> _getSuggestion(List<CountryModel> list, String query) {
    // ignore: deprecated_member_use
    List<String> matches = List();

    for (var item in list) {
      matches.add(item.country);
    }

    matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return matches;
  }

  @override
  void initState() {
    super.initState();
    countryList = covidServices.getCountryList();
    this._typeController.text = "Indonesia";
    summaryList = covidServices.getCountrySummary("Indonesia");
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: countryList,
      builder: (context, snapshot) {
        if (snapshot.hasError)
          return Center(
            child: Text("Error"),
          );

        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return Container();
            break;
          default:
            return !snapshot.hasData
                ? Center(
                    child: Text(
                      "Empty",
                      style: defaultTextStyle,
                    ),
                  )
                : Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          left: 25,
                          right: 25,
                          bottom: 30,
                        ),
                        child: Text(
                          "You can search here",
                          style: defaultTextStyle.copyWith(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 25,
                          right: 25,
                        ),
                        child: TypeAheadFormField(
                          textFieldConfiguration: TextFieldConfiguration(
                            controller: _typeController,
                            decoration: InputDecoration(
                              hintText: "Type Your Country Here",
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(15),
                                borderSide: BorderSide(
                                  width: 0,
                                  style: BorderStyle.none,
                                ),
                              ),
                              filled: true,
                              fillColor: Colors.grey[200],
                              contentPadding: EdgeInsets.all(20),
                              prefixIcon: Padding(
                                padding:
                                    EdgeInsets.only(left: 24.0, right: 16.0),
                                child: Image.asset(
                                  "assets/search-color.png",
                                  width: 25,
                                ),
                              ),
                            ),
                          ),
                          suggestionsCallback: (pattern) {
                            return _getSuggestion(snapshot.data, pattern);
                          },
                          itemBuilder: (context, suggestion) {
                            return ListTile(
                              title: Text(suggestion),
                            );
                          },
                          onSuggestionSelected: (suggestion) {
                            this._typeController.text = suggestion;
                            setState(() {
                              summaryList = covidServices.getCountrySummary(
                                  snapshot
                                      .data
                                      .firstWhere((element) =>
                                          element.country == suggestion)
                                      .slug);
                            });
                          },
                        ),
                      ),
                      FutureBuilder(
                        future: summaryList,
                        builder: (context, snapshot) {
                          if (snapshot.hasError)
                            return Center(
                              child: Text("Error"),
                            );

                          switch (snapshot.connectionState) {
                            case ConnectionState.waiting:
                              return Container(
                                margin: EdgeInsets.only(
                                  top: 30,
                                ),
                                child: CircularProgressIndicator(
                                  backgroundColor: Colors.blue,
                                  color: Colors.white,
                                ),
                              );
                              break;
                            default:
                              return !snapshot.hasData
                                  ? Center(child: Text("empty"))
                                  : CardSearchPage(
                                      summaryList: snapshot.data,
                                    );
                          }
                        },
                      )
                    ],
                  );
        }
      },
    );
  }
}
