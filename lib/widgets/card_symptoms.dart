import 'package:covid_app/utils/theme.dart';
import 'package:flutter/material.dart';

class CardSymptoms extends StatefulWidget {
  final String picture;
  final String title;

  CardSymptoms(this.picture, this.title);

  @override
  _CardSymptomsState createState() => _CardSymptomsState();
}

class _CardSymptomsState extends State<CardSymptoms> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 1),
            blurRadius: 1,
            color: shadowColor,
          ),
        ],
      ),
      child: Column(
        children: [
          Image.asset(
            widget.picture,
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            widget.title,
            style: defaultTextStyle,
          )
        ],
      ),
    );
  }
}
