import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class LoadingShimmer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget loadingShimmer() {
      return Card(
        elevation: 1,
        child: Container(
          height: 70,
          padding: EdgeInsets.only(
            top: 5,
            left: 10,
            right: 10,
            bottom: 10,
          ),
          child: Shimmer.fromColors(
            baseColor: Colors.grey[300],
            highlightColor: Colors.grey[100],
            child: Column(
              children: [
                Container(
                  width: double.infinity,
                  height: 10,
                  color: Colors.white,
                ),
                Expanded(
                  child: Container(),
                ),
                Container(
                  width: double.infinity,
                  height: 5,
                  color: Colors.white,
                ),
                SizedBox(
                  height: 5,
                ),
                Container(
                  width: double.infinity,
                  height: 5,
                  color: Colors.white,
                ),
              ],
            ),
          ),
        ),
      );
    }

    return Column(
      children: [
        loadingShimmer(),
        loadingShimmer(),
        loadingShimmer(),
        loadingShimmer(),
      ],
    );
  }
}
