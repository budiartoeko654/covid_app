import 'package:covid_app/pages/covid_education.dart';
import 'package:covid_app/pages/home_page.dart';
import 'package:covid_app/pages/search_page.dart';
import 'package:flutter/material.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    Widget bottomNavbar() {
      return BottomNavigationBar(
        backgroundColor: Colors.white,
        currentIndex: currentIndex,
        onTap: (value) {
          setState(() {
            currentIndex = value;
          });
        },
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: Container(
              padding: EdgeInsets.only(top: 10),
              child: Image(
                image: currentIndex == 0
                    ? AssetImage("assets/bacteria-color.png")
                    : AssetImage("assets/bacteria.png"),
                width: 25,
              ),
            ),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Container(
              padding: EdgeInsets.only(top: 10),
              child: Image(
                image: currentIndex == 1
                    ? AssetImage("assets/search-color.png")
                    : AssetImage("assets/search.png"),
                width: 25,
              ),
            ),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Container(
              padding: EdgeInsets.only(top: 10),
              child: Image(
                image: currentIndex == 2
                    ? AssetImage("assets/test-results-color.png")
                    : AssetImage("assets/test-results.png"),
                width: 25,
              ),
            ),
            label: '',
          ),
        ],
      );
    }

    Widget body() {
      switch (currentIndex) {
        case 0:
          return HomePage();
          break;
        case 1:
          return SearchPage();
          break;
        case 2:
          return CovidEducation();
          break;
        default:
          return HomePage();
      }
    }

    return Scaffold(
      bottomNavigationBar: bottomNavbar(),
      body: body(),
    );
  }
}
