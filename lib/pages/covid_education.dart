import 'package:covid_app/utils/theme.dart';
import 'package:covid_app/widgets/card_prevention.dart';
import 'package:covid_app/widgets/card_symptoms.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CovidEducation extends StatefulWidget {
  @override
  _CovidEducationState createState() => _CovidEducationState();
}

class _CovidEducationState extends State<CovidEducation> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          ClipPath(
            clipper: Clipper(),
            child: Container(
              width: double.infinity,
              height: 260,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [
                    Color(0xFF33B3CD),
                    Color(0xFF11249F),
                  ],
                ),
                image: DecorationImage(
                  image: AssetImage(
                    "assets/virus.png",
                  ),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 55,
                  ),
                  Expanded(
                    child: Stack(
                      children: [
                        SvgPicture.asset(
                          "assets/coronadr.svg",
                          width: 260,
                          fit: BoxFit.fitWidth,
                          alignment: Alignment.topCenter,
                        ),
                        Positioned(
                          top: 25,
                          left: 165,
                          child: Text(
                            "Let's Find Out\nWhat Covid 19 is",
                            style: defaultTextStyle.copyWith(
                              fontSize: 20,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Container(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              left: 25,
              right: 25,
            ),
            child: Text(
              "Common Symptoms",
              style: defaultTextStyle.copyWith(
                fontSize: 18,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              left: 25,
              right: 25,
              top: 20,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                CardSymptoms(
                  "assets/headache.png",
                  "Headache",
                ),
                CardSymptoms(
                  "assets/fever.png",
                  "Fever",
                ),
                CardSymptoms(
                  "assets/caugh.png",
                  "caugh",
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              left: 25,
              top: 20,
              right: 25,
            ),
            child: Text(
              "Prevention",
              style: defaultTextStyle.copyWith(
                fontSize: 18,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          CardPrevention(
            "assets/wash_hands.png",
            "Wash Your Hand",
            "Don't forget to always wash your hands",
          ),
          CardPrevention(
            "assets/wear_mask.png",
            "Wear Face mask",
            "Don't forget to wear a mask wherever you go",
          ),
          SizedBox(
            height: 15,
          ),
        ],
      ),
    );
  }
}

class Clipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.lineTo(0, size.height - 80);
    path.quadraticBezierTo(
        size.width / 2, size.height, size.width, size.height - 80);
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => false;
}
