import 'package:covid_app/models/global_summary.dart';
import 'package:covid_app/services/covid_services.dart';
import 'package:covid_app/utils/theme.dart';
import 'package:covid_app/widgets/card_covid.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

CovidServices covidServices = CovidServices();

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Future<GlobalSummaryModel> summary;

  @override
  void initState() {
    super.initState();
    summary = covidServices.getGlobalSummary();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF6F7F9),
      body: Builder(
        builder: (BuildContext context) {
          return Column(
            children: [
              Expanded(
                flex: 2,
                child: ListView(
                  children: [
                    ClipPath(
                      clipper: Clipper(),
                      child: Container(
                        width: double.infinity,
                        height: 260,
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                            colors: [
                              Color(0xFF33B3CD),
                              Color(0xFF11249F),
                            ],
                          ),
                          image: DecorationImage(
                            image: AssetImage(
                              "assets/virus.png",
                            ),
                          ),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 55,
                            ),
                            Expanded(
                              child: Stack(
                                children: [
                                  SvgPicture.asset(
                                    "assets/Drcorona.svg",
                                    width: 240,
                                    fit: BoxFit.fitWidth,
                                    alignment: Alignment.topCenter,
                                  ),
                                  Positioned(
                                    top: 40,
                                    left: 150,
                                    child: Text(
                                      "Stay Healthy,\nWherever You Are",
                                      style: defaultTextStyle.copyWith(
                                        fontSize: 20,
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                  Container(),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                        left: 25,
                        right: 25,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Global Corona Virus Cases",
                            style: defaultTextStyle.copyWith(
                              fontSize: 18,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 2,
                child: Container(
                  color: Color(0xFFF6F7F9),
                  margin: EdgeInsets.only(
                    left: 30,
                    right: 30,
                  ),
                  child: CardCovid(),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}

class Clipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.lineTo(0, size.height - 80);
    path.quadraticBezierTo(
        size.width / 2, size.height, size.width, size.height - 80);
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => false;
}
