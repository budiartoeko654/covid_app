import 'package:covid_app/utils/theme.dart';
import 'package:covid_app/widgets/card_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:covid_app/services/covid_services.dart';

CovidServices covidServices = CovidServices();

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  @override
  Widget build(BuildContext context) {
    return Builder(builder: (BuildContext context) {
      return Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.bottomLeft,
            end: Alignment.topRight,
            colors: [
              Color(0xFF33B3CD),
              Color(0xFF11249F),
            ],
          ),
        ),
        child: Column(
          children: [
            Expanded(
              flex: 2,
              child: ListView(
                children: [
                  ClipPath(
                    clipper: Clipper(),
                    child: Container(
                      width: double.infinity,
                      height: 260,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                          colors: [
                            Color(0xFF33B3CD),
                            Color(0xFF11249F),
                          ],
                        ),
                        image: DecorationImage(
                          image: AssetImage(
                            "assets/virus.png",
                          ),
                        ),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 50,
                          ),
                          Expanded(
                            child: Stack(
                              children: [
                                SvgPicture.asset(
                                  "assets/virus.svg",
                                  width: 160,
                                  fit: BoxFit.fitWidth,
                                  alignment: Alignment.topCenter,
                                ),
                                Positioned(
                                  top: 20,
                                  left: 165,
                                  child: Text(
                                    "How Is Covid\nAround The World",
                                    style: defaultTextStyle.copyWith(
                                      fontSize: 20,
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                Container(),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 3,
              child: Container(
                child: CardSearch(),
              ),
            )
          ],
        ),
      );
    });
  }
}

class Clipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.lineTo(0, size.height - 80);
    path.quadraticBezierTo(
        size.width / 2, size.height, size.width, size.height - 80);
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => false;
}
