import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'dart:core';

TextStyle defaultTextStyle = GoogleFonts.poppins();

Color blueColor = Colors.blue;
Color redColor = Colors.red;
Color greenColor = Colors.green;
Color greyColor = Colors.grey[700];
Color shadowColor = Color(0xFFB7B7B7).withOpacity(.9);

RegExp reg = new RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))');
Function mathFunc = (Match match) => '${match[1]}.';
